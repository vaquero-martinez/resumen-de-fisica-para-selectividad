# Luz, óptica y física moderna

## Luz

### Índice de refracción (n)

Se denomina *índice de refracción* de un medio material a la relación entre la velocidad de la luz en el vacío (c) y la velocidad en dicho medio (v)

$$n = c/v > 1$$

Cuando la luz cambia de medio su frecuencia **no varía**, ya que esta solo depende del centro emisor, y sin embargo la velocidad sí, y por la relación $v=\lambda f$ la longitud de onda también cambia. Así:

$$n = c/v = \lambda_0 f/(\lambda f) = \lambda_0/\lambda$$

### Leyes de Snell

Las leyes de Snell son las dos siguientes (Recordar la Figura \@ref(fig:snell))

* **Reflexión**. El rayo incidente, la normal y el rayo reflejado se encuentran en el mismo plano. El ángulo de incidencia es igual al de reflexión.

* **Refracción**.  El rayo incidente, la normal y el rayo refractado se encuentran en el mismo plano. La relación entre el seno del ángulo de incidencia y el seno del ángulo de refracción es una constante característica de los dos medios: $$n_i \sin \hat{i} = n_r \sin \hat{r} \Rightarrow \frac{\sin \hat{i}}{\sin \hat{r}} = \frac{n_r}{n_i} = \frac{v_i}{v_r}$$

Las leyes de Snell, la propagación rectilínea de la luz y otros fenómenos relacionados con la propagación de la luz pueden deducirse del **Principio de Fermat**: 

> De todas las trayectorias posibles, la trayectoria real de la luz es aquella que se recorre en el mínimo tiempo.

### Consecuencia de las leyes de Snell: ángulo límite y reflexión total.

Consideremos un punto luminoso $P$, situado en un medio de índice de refracción $n_1$, que emite una serie de rayos luminosos los cuales se refractan al pasar a otro medio de índice de refracción $n_2$, tal que $n_2 < n_1$. La Figura \@ref(fig:alimite) muestra un esquema de esta situación.

Por la ley de Snell de la refracción, los rayos refractados se alejan de la normal, por lo que a medida que aumentemos el ángulo de incidencia el ángulo refractado se acerca a los $90^{\circ}$ hasta que para un ángulo $\hat{l}$, llamado **ángulo límite**, el ángulo de refracción serán $90^{\circ}$. $$\frac{\sin \hat{i}}{\sin 90^{\circ}} = \frac{n_2}{n_1} \Rightarrow \sin \hat{l} = n_2/n_1$$

Para ángulos de incidencia mayores solo existe refracción (el ángulo de refracción sería mayor de $90^{\circ}$, quedando el rayo en el primer medio). A este fenómeno se le llama **reflexión total**.

```{r alimite, fig.cap="Ángulo límite y reflexión total."}
knitr::include_graphics(paste0(image.path, "Refraccion_y_reflexion.svg"), auto_pdf = TRUE)
```

## Óptica

* **Espejos** $$\frac{1}{s'} + \frac{1}{s} = \frac{2}{R}$$
$$f = f' = R/2$$
$$A_L= y'/y = - s'/s$$

```{r espejos, fig.cap="Ejemplo de esquema de rayos para los espejos."}
knitr::include_graphics(paste0(image.path, "espejo.svg"), auto_pdf = TRUE)
```

Rayos (ver Figura \@ref(fig:espejos)):

1) Rayo paralelo $\rightarrow$ foco
2) Rayo foco $\rightarrow$ paralelo
3) Centro $\rightarrow$ no se desvía.

* **Lentes** $$\frac{1}{s'} - \frac{1}{s} = (n-1) \left( \frac{1}{R_1} - \frac{1}{R_2}  \right) = \frac{1}{f' } = -\frac{1}{f} = P$$
$$A_L= y'/y = s'/s$$

```{r lentes, fig.cap="Ejemplo de esquema de rayos para las lentes.  Nota: Falta el rayo 2), que pasa por el foco y sale paralelo (en la imagen se saldría de la lente)."}
knitr::include_graphics(paste0(image.path, "lentes.svg"), auto_pdf = TRUE)
```

Rayos (ver Figura \@ref(fig:lentes)):

1) Rayo paralelo $\rightarrow$ foco imagen
2) Rayo foco objeto $\rightarrow$ paralelo
3) Origen $\rightarrow$ no se desvía.

## Física relativista

**Postulados de la Teoría de la Relatividad (Einstein)**:

> 1. Las leyes físicas son válidas  tienen la misma expresión matemática entodos los sistemas de referncia inerciales.

(Este postulado generaliza el de la relatividad de Galileo, que solo se aplicaba a la Mecánica, y no al resto de leyes físicas, como la Electrodinámica o la Óptica)

> 2. La velocidad de la luz es la misma en todos los sistema de referencia inerciales. En otras palabras, la velocidad es la misma cualesquiera que sean los movimientos del foco y el observador. 

(Esto lleva a buscar otras leyes de transformación, que serán las llamadas **Transformaciones de Lorentz**, que llevan a la dilatación del tiempo y la contracción de longitudes)

### Energía relativista

Las transformaciones de Lorentz llevan que la masa de un cuerpo es diretamente proporcional a su energía, es decir, energía y masa son dos aspectos de la misma propiedad de los entes físicos. La energía total de un cuerpo será:

$$E = mc^2 = \gamma m_0 c^2 = \frac{m_0 c^2}{\sqrt{
1-v^2/c^2}}$$

Otra forma de ver esta expresión es:

$$E^2 = (m_0 c^2 )^2 + (pc)^2$$

cuya interpretación gráfica sería que la energía en reposo (primer sumando) y la energía cinética (segundo sumando) son los catetos de un triángulo rectángulo en el que la hipotenusa es la energía total.

## Física Cuántica

### Radiación térmica y teoría de Planck

La radiación térmica es la radiación electromagnética que emite un cuerpo por el hecho de tener temperatura. Se conoce como **cuerpo negro** aquel que es capaz de absorber todas las radiaciones que llegan a él, y por tanto, de emitir todas las longitudes de onda.

Para cuantificar la radiación del cuerpo negro existen dos leyes experimentales:

- Ley de Wien: $\lambda_{\text{máx}} T = 2.9 \cdot 10^{-3} \mathrm{m K}~(\text{cte.})$
- Ley de Stefan-Boltzmann: $\underbrace{I}_{\text{Intensidad\ total}}=\sigma T^4$

La teoría clásica predecía que la intensidad debería aumentar indefinidamente al disminuir la longitud de onda, lo que haría que la intensidad tendiera a infinito. Los resultados experimentales comenzaban a diferir de la teoría en la región del ultravioleta. Por eso, a esta predicción errónea se la conoce como la **catástrofe ultravioleta**.

**Hipótesis de Planck**: Planck resolvió este problema suponiendo que la energía no se emite de forma continua, sino en paquetes (cuantos) de energía de frecuencia determinada. La energía de cada cuanto sería $E=hf$. Esta suposición da lugar a la **Ley de radiación de Planck**, que reproduce correctamente el comportamiento del cuerpo negro.

### Efecto fotoeléctrico

Consiste en la emisión de electrones (*fotoelectrones*) por la superficie de un metal cuando se ilumina con luz de la frecuencia adecuada.

* **Características**:
  - Cuando hacemos V negativo la corriente decrece bruscamente, siendo nula para el **potencial de frenado** ($V_0$) que depende de la frecuencia de la radiación pero no de la intensidad de la misma. $$\frac{1}{2} m v_{\text{máx}}^2 = e V_0$$
	- Para V postivo la corriente fotoeléctrica alcanza un valor máximo (de saturación, $i_s$), que aumenta con la intensidad luminosa.
	- Para cada metal existe una **frecuencia umbral** por debajo de la cual no hay efecto fotoeléctrico (independientemente de la intensidad de la luz).
	- Solo cuando $f>f_{\text{umbral}}$ la intensidad de corriente es proporcional a la intensidad de la luz. 
	- La $E_c$ de los electrones aumenta con la frecuencia de la luz, pero es independiente de la intensidad luminosa. 
	- La emisión de fotoelectrones es instantánea (menos de $10^{-9} \mathrm{s}$)
	
* **Teoría de Einstein**:

Einstein propuso que la luz se propaga en *cuantos* de luz (fotones), cuya energía es $E=hf$. Toda la energía del fotón pasa a un electrón del metal.
$$\underbrace{hf}_{\text{Energía del fotón}} = \overbrace{W_0}^{ \text{función de trabajo}} + \underbrace{\frac{1}{2} m v^2}_{\text{Energía cinética del electrón}}$$

La función de trabajo (o trabajo de extracción) es la energía necesaria para que el electrón sea capaz de escapar del pozo de potencial en el que se encuentra dentro del metal. La frecuencia umbral es la mínima para arrancar electrones del metal $$hf_0 = W_0$$

### Modelo de Bohr

Propone órbitas definidas. Tiene tres postulados.

> 1) Existe una serie de órbitas en las que el electrón no emite energía.
> 2) El momento angular de dichas órbitas cumple: $$L = mvr = n h/2\pi = n\hbar$$
> 3) Cuando un electrón pasa de una órbita a otra, lo hace emitiendo o absorbiendo un fotón. $$\left| E_2 - E_1 \right| = hf$$

Así logra explicar las líneas espectrales del hidrógeno: $$\frac{1}{\lambda} = R \left( \frac{1}{n_1^2} - \frac{1}{n_2^2} \right)$$

### Hipótesis de De Broglie: Dualidad onda-corpúsculo

> A toda partícula en movimiento le corresponde una onda cuya longitud de onda es inversamente proporcional al momento lineal de esa partícula $$\lambda = h/p = \frac{h}{mv}$$

### Principio de Incertidumbre de Heisemberg

> No es posible conocer simultáneamente y con exactitud la posición de una partícula ($x$) y  su momento lineal ($p$). El producto de sus indeterminaciones al medir simultáneamente dichas magnitudes siempre es mayor que $\hbar$. $$\Delta x \Delta p \geq \hbar$$ $$\Delta E \Delta t \geq \hbar$$

Esto permite la existencia en el mundo cuántico del *efecto túnel* y el *vacío cuántico*.

### Modelo de Schrödinger. Ecuación de Schrödinger

Las órbitas no son definidas. El electrón pasa a ser una nube de carga donde es posible encontrar al electrón. A la superficie límite que contiene un volumen en el que se puede encontrar al electrón con una probabilidad del $90-99\%$ se le llama **orbital**.

$$\nabla^{2} \Psi + 8\pi^2 \frac{m}{h^2} (E-V) \Psi = 0$$

$\Psi$ onda asociada a la partícula (electrón).

## Física Nuclear 

### Números:
* Atómico (Z): número de protones
* Másico (A): número de nucleones (protones y neutrones)
	
$$^A_ZX$$
	
Los isótopos tienen el mismo Z pero distinto A

### Radiactividad

Tres tipos de radiación:

1) Radiación $\alpha$: un núcleo grande es inestable por la repulsión de los protones y emite una partícula $\alpha$, que es un núcleo de $^4_{2}\mathrm{He}$. Es poco penetrante.

$$ ^A_ZX \rightarrow ^{A-4}_{Z-2} Y + \underbrace{^4_{2}\mathrm{He}}_{\alpha} $$

2) Radiación $\beta$: si la relación neutrones/protones es muy grande, un neutrón puede convertirse en protón emitiendo un electrón (partícula $\beta$). Es más penetrante.

$$^A_ZX \rightarrow ^{A}_{Z+1} Y + \underbrace{^{0}_{-1}e}_{\beta} + \underbrace{\bar{\nu}_{e}}_{\text{antineutrino}}$$

3) Radiación $\gamma$: son ondas electromagnéticas (fotones) a muy alta frecuencia, superiores a los rayos X. $$^A_ZX^* \leftarrow ^A_ZX + \gamma$$

#### Desintegración y magnitudes

$$-dN = \lambda N dt \Rightarrow N = N_0 e^{-\lambda t} \Rightarrow m = m_0 e^{-\lambda t}$$

**Actividad**: número de desintegraciones por unidad de tiempo.

$$A = \lambda N~ (\text{Unidad Becquerel (Bq)})$$
$$A = A_0 e^{-\lambda t}$$

* Periodo de semidesintegración ($T_{1/2}$): tiempo para que $N = N_0/2$. $$N_0/2 = N_0 e^{-\lambda T_{1/2}} \Rightarrow T_{1/2} = \ln 2 / \lambda$$

* Vida media ($\tau$): tiempo por término medio que tarda un núcleo en desintegrarse $$\tau = 1/\lambda = T_{1/2} /\ln 2$$

#### Fisión nuclear

Consiste en la división de un núcleo pesado en dos más ligeros que son más estables. Ocurre al absorber un neutrón y se obtiene más neutrones. La energía liberada son unos 200 MeV por núcleo, mucho mayor que en la combustión. En un reactor se producen reacciones en cadena, controladas con barras de control, que absorben neutrones.

Entre las desventajas de este tipo de energía se encuentra la necesidad de utilizar materia prima como el uranio, que no es muy abundante, la producción de residuos radiactivos y la posibilidad de accidentes nucleares que tendrían efectos devastadores en la zona en la que se encuentre la central nuclear. Por otro lado, a parte de los residuos radiactivos, el impacto ambiental es pequeño porque no se producen gases invernadero como en la combustión, y con la misma cantidad de material se puede producir más energía que en el caso de la combustión.

#### Fusión nuclear

Consiste en la unión de núcleos ligeros para formar núcleos más pesados y estables, liberando energía. Se liberan aproximadamente 17.6 MeV por núcleo. 

Son necesarias temperaturas muy altas ($10^9 \mathrm{K}$), formando un gas de cationes y electrones, llamado plasma. Para confinarlo (ningún material resiste esas temperaturas) se recurriría al confinamiento magnético e inercial.

El confinamiento **magnético** consiste en utilizar campos eléctricos ymagnéticos para atrapar el plasma, mientras que el **inercial** trata de conseguir altas densidades, mucho mayores que en el estado sólido, durante periodos cortos de tiempo, calentando pequeñas cápsulas mediante rayos láser.

Presenta indudables ventajas: materia prima abundante, y barata, menos residuos radiactivos, y mayor seguridad. Sin embargo, hay dificultades científicas y tecnológicas enormes y su uso a corto plazo no podrá ocurrir hasta dentro de mucho tiempo.
