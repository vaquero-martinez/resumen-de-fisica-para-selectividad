# Resumen de Física para Selectividad

Este libro corresponde a unas clases de di para preparar selectividad. Trata de resumir los puntos más importantes de la Física de Segundo de Bachillerato en España (en Extremadura en concreto). Es verdad que han pasado algunos años desde entonces y puede estar desactualizado. En tal caso siempre se agradece que se informe, y cuando tenga tiempo lo actualizaré.

Acceso al libro en: [https://gitlab.com/vaquero-martinez/resumen-de-fisica-para-selectividad](https://gitlab.com/vaquero-martinez/resumen-de-fisica-para-selectividad)