#!/bin/bash

# Poner el locale en castellano
touch /usr/share/locale/locale.alias
sed -i -e 's/# \(es_ES.UTF-8 .*\)/\1/' /etc/locale.gen && \
    locale-gen
export LANG=es_ES.UTF-8  
export LANGUAGE=es_ES:es  
export LC_ALL=es_ES.UTF-8  


curl -fsSL https://www.preining.info/rsa.asc | tlmgr key add -
# apt -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install texlive texlive-xetex 

# Rscript -e "tinytex::reinstall_tinytex()"

# tlmgr install xpatch

cd $(dirname $0)

### copy everything to a tmp dir
rm -rf tmp/
rsync -a . tmp/
cd tmp/

### preprocess markdown files to fix them for pandoc
for file in $(find . -name '*.md'); do
    # fix latex delimiters for pandoc
    sed -i $file \
        -e 's/$`/$/g' \
        -e 's/`\$/$/g'

    # leave an empty line before starting a list, otherwise pandoc does not interpret it as a list
    sed -i $file \
        -e '/./{H;$!d} ; x ; s/:\n-/:\n\n-/'

    # open links on a new tab
    sed -i $file \
        -e 's#\([^(]\)\(https\?://.*\)#\1[\2](\2){target="_blank"}#'
done

### render all the formats
rm -rf ../public/
Rscript -e "bookdown::render_book('index.Rmd', 'all', output_dir = '../public')"

### copy font Monaco
cd ..
cp assets/Monaco.woff public/

### clean up
rm -rf tmp/
